package dk.itu.moapd.bikeshare.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import dk.itu.moapd.bikeshare.entities.Bike
import dk.itu.moapd.bikeshare.entities.Ride
import dk.itu.moapd.bikeshare.repositories.BikeRepository
import dk.itu.moapd.bikeshare.repositories.RideRepository
import java.io.File
import java.util.*

class BikeVM() : ViewModel() {

    private val bikeRepository: BikeRepository = BikeRepository.get()
    private val rideRepository: RideRepository = RideRepository.get()

    val bikeListLiveData = bikeRepository.getBikes()
    val bikesWithRidesLiveData = bikeRepository.getBikesWithRides()

    val activeRideListLiveData = rideRepository.getActiveRides()

    fun addRide(ride: Ride) {
        rideRepository.addRide(ride)
    }

    fun saveRide(ride: Ride) {
        rideRepository.updateRide(ride)
    }

    fun getPhotoFile(bike: Bike): File {
        return bikeRepository.getPhotoFile(bike)
    }

    fun getLat(bike: Bike): Double {
        return bikeRepository.getLat(bike)
    }

    fun getLng(bike: Bike): Double {
        return bikeRepository.getLng(bike)
    }

    fun getBike(uuid: UUID): LiveData<Bike?> {
        return bikeRepository.getBike(uuid)
    }

    fun saveBike(bike: Bike) {
        bikeRepository.updateBike(bike)
    }

    fun addBike(bike: Bike) {
        bikeRepository.addBike(bike)
    }

    fun deleteBike(bike: Bike) {
        bikeRepository.deleteBike(bike)
    }

    fun getAllBikes(): LiveData<List<Bike>> =
        bikeRepository.getBikes()
}