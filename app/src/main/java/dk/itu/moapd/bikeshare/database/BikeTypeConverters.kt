package dk.itu.moapd.bikeshare.database

import androidx.room.TypeConverter
import dk.itu.moapd.bikeshare.entities.Type
import java.util.*

class BikeTypeConverters {

    @TypeConverter
    fun toUUID(uuid: String?): UUID? {
        return UUID.fromString(uuid)
    }

    @TypeConverter
    fun fromUUID(uuid: UUID?): String? {
        return uuid?.toString()
    }

    @TypeConverter
    fun toType(type: Int?): Type? {
        return Type(type!!)
    }

    @TypeConverter
    fun fromType(type: Type?): Int? {
        return type?.type
    }

}