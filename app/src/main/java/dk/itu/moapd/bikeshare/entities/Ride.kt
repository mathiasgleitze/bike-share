package dk.itu.moapd.bikeshare.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Ride(@PrimaryKey val rideId: UUID = UUID.randomUUID(),
                @ColumnInfo(index = true)
                var relatedBikeId: UUID,
                var startLat: Double,
                var startLng: Double,
                var startTime: Long,
                var endLat: Double? = 0.0,
                var endLng: Double? = 0.0,
                var endTime: Long? = 0,
                var price: Int? = 0)