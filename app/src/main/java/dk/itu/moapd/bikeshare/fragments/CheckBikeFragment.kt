package dk.itu.moapd.bikeshare.fragments

import android.annotation.SuppressLint
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.hdodenhof.circleimageview.CircleImageView
import dk.itu.moapd.bikeshare.R
import dk.itu.moapd.bikeshare.entities.Bike
import dk.itu.moapd.bikeshare.entities.BikeWithRides
import dk.itu.moapd.bikeshare.entities.Ride
import dk.itu.moapd.bikeshare.utilities.ThumbnailMaker
import dk.itu.moapd.bikeshare.utilities.getDate
import dk.itu.moapd.bikeshare.viewModels.BikeVM
import java.io.File

class CheckBikeFragment: Fragment() {

    private lateinit var bikeRecyclerView: RecyclerView
    private lateinit var adapter: CheckBikeOuterAdapter
    private lateinit var thumbnailMaker: ThumbnailMaker<CheckBikeOuterHolder>
    private lateinit var filesPath: String


    private val bikeVM: BikeVM by lazy {
        ViewModelProviders.of(this).get(BikeVM::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        filesPath = context!!.applicationContext.filesDir.path
        retainInstance = true // This is generally not good.
        val responseHandler = Handler()
        thumbnailMaker =
            ThumbnailMaker(responseHandler) { photoHolder, bitmap ->
                val drawable = BitmapDrawable(resources, bitmap)
                photoHolder.bindDrawable(drawable)
            }
        lifecycle.addObserver(thumbnailMaker.fragmentLifecycleObserver)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewLifecycleOwner.lifecycle.removeObserver(thumbnailMaker.viewLifecycleObserver)
    }

    override fun onDestroy() {
        super.onDestroy()
        lifecycle.removeObserver(thumbnailMaker.fragmentLifecycleObserver)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_check_bike, container, false)
        bikeRecyclerView = view.findViewById(R.id.bike_recycler_view) as RecyclerView
        bikeRecyclerView.layoutManager = LinearLayoutManager(context)
        bikeRecyclerView.adapter = CheckBikeOuterAdapter(emptyList())
        viewLifecycleOwner.lifecycle.addObserver(thumbnailMaker.viewLifecycleObserver)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.check_bike)
        bikeVM.bikesWithRidesLiveData.observe(
            viewLifecycleOwner,
            Observer { bikeWithRides: List<BikeWithRides> ->
                updateUI(bikeWithRides)
            })
    }

    private fun updateUI(bikeWithRides: List<BikeWithRides>) {
        adapter = CheckBikeOuterAdapter(bikeWithRides)
        bikeRecyclerView.adapter = adapter
    }

    private inner class CheckBikeOuterAdapter(private var bikeWithRides: List<BikeWithRides>): RecyclerView.Adapter<CheckBikeOuterHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckBikeOuterHolder {
            val view = layoutInflater.inflate(R.layout.recyclerview_check_bike_outer, parent, false)
            return CheckBikeOuterHolder(view)
        }

        override fun getItemCount() = bikeWithRides.size

        override fun onBindViewHolder(holder: CheckBikeOuterHolder, position: Int) {
            val bikeWithRide = bikeWithRides[position]
            holder.bind(bikeWithRide)
            thumbnailMaker.queueThumbnail(holder, filesPath + "/" + bikeWithRide.bike.photoFileName)
        }
    }


    private inner class CheckBikeOuterHolder(view: View): RecyclerView.ViewHolder(view) {
        private lateinit var bike: Bike
        private lateinit var photoFile: File

        private val typeTextView: TextView = itemView.findViewById(R.id.bike_type)
        private val priceTextView: TextView = itemView.findViewById(R.id.bike_price)
        private val image: CircleImageView = itemView.findViewById(R.id.image)
        private val viewMoreBtn: ImageButton = itemView.findViewById(R.id.viewMoreButton)
        private val layoutExpand: LinearLayout = itemView.findViewById(R.id.layoutExpand)
        private val innerRecyclerView: RecyclerView = itemView.findViewById(R.id.inner_recycler_view)
        private var viewMoreBtnClicked: Boolean = false

        val bindDrawable: (Drawable) -> Unit  = image::setImageDrawable

        @SuppressLint("SetTextI18n")
        fun bind(bikeWithRide: BikeWithRides) {
            this.bike = bikeWithRide.bike
            this.photoFile = bikeVM.getPhotoFile(bike)

            typeTextView.text = bike.type.toString()
            priceTextView.text = getString(R.string.bike_price) + " " + bike.price.toString()
            image.setImageResource(R.drawable.bike)

            if (bikeWithRide.Rides.isEmpty()) {
                viewMoreBtn.visibility = View.INVISIBLE
            } else {
                innerRecyclerView.layoutManager = LinearLayoutManager(activity)
                viewMoreBtn.setOnClickListener { view ->
                    view?.let {
                        viewMoreBtnClicked = !viewMoreBtnClicked
                        if (viewMoreBtnClicked) {
                            view.animate().setDuration(150).rotation(90f)
                            val adapter = CheckBikeInnerAdapter(bikeWithRide.Rides)
                            innerRecyclerView.adapter = adapter
                            layoutExpand.visibility = View.VISIBLE
                        } else {
                            view.animate().setDuration(150).rotation(0f)
                            layoutExpand.visibility = View.GONE
                        }
                    }
                }
            }
        }
    }

    private inner class CheckBikeInnerAdapter(private val rides: List<Ride>): RecyclerView.Adapter<CheckBikeInnerHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckBikeInnerHolder {
            val view = layoutInflater.inflate(R.layout.recyclerview_check_bike_inner, parent, false)
            return CheckBikeInnerHolder(view)
        }

        override fun getItemCount() = rides.size

        override fun onBindViewHolder(holder: CheckBikeInnerHolder, position: Int) {
            val ride = rides[position]
            holder.bind(ride)
        }
    }

    private inner class CheckBikeInnerHolder(view: View): RecyclerView.ViewHolder(view) {
        lateinit var ride: Ride
        private val startTimeTextView: TextView = itemView.findViewById(R.id.ride_start_time)
        private val endTimeTextView: TextView = itemView.findViewById(R.id.ride_end_time)
        private val priceTextView: TextView = itemView.findViewById(R.id.ride_price)

        fun bind(ride: Ride) {
            this.ride = ride
            if (ride.price != 0) {
                startTimeTextView.text = getString(R.string.from).plus(" ").plus(getDate(ride.startTime))
                endTimeTextView.text = getString(R.string.to).plus(" ").plus(getDate(ride.endTime!!))
                priceTextView.text = getString(R.string.paid).plus(" ").plus(ride.price)
            } else {
                startTimeTextView.text = getString(R.string.ongoing).plus(" ").plus(getDate(ride.startTime))
                endTimeTextView.visibility = View.GONE
                priceTextView.visibility = View.GONE
            }
        }
    }
}
