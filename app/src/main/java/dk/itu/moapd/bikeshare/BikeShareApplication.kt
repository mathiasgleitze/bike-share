package dk.itu.moapd.bikeshare

import android.app.Application
import dk.itu.moapd.bikeshare.repositories.BikeRepository
import dk.itu.moapd.bikeshare.repositories.RideRepository

class BikeShareApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        BikeRepository.initialize(this)
        RideRepository.initialize(this)
    }
}
