package dk.itu.moapd.bikeshare.database

import androidx.lifecycle.LiveData
import androidx.room.*
import dk.itu.moapd.bikeshare.entities.Bike
import dk.itu.moapd.bikeshare.entities.BikeWithRides
import java.util.*

@Dao
interface BikeDao {

    @Query("SELECT * FROM bike")
    fun getBikes(): LiveData<List<Bike>>

    @Query("SELECT * FROM bike WHERE bikeId=(:bikeId)")
    fun getBike(bikeId: UUID): LiveData<Bike?>

    @Transaction
    @Query("SELECT * FROM bike")
    fun getBikesWithRides(): LiveData<List<BikeWithRides>>

    @Update
    fun updateBike(bike: Bike)

    @Insert
    fun addBike(bike: Bike)

    @Delete
    fun deleteBike(bike: Bike)

}