package dk.itu.moapd.bikeshare.utilities

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Handler
import android.os.HandlerThread
import android.os.Message
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import java.util.concurrent.ConcurrentHashMap

private const val TAG = "ThumbnailMaker"
private const val MESSAGE_MAKE = 0


class ThumbnailMaker<in T>(
    private val responseHandler: Handler,
    private val onThumbnailMade: (T, Bitmap) -> Unit
): HandlerThread(TAG) {

    val fragmentLifecycleObserver: LifecycleObserver =
        object : LifecycleObserver {

            @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
            fun setup() {
                start()
                looper
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun tearDown() {
                quit()
            }
        }

    val viewLifecycleObserver: LifecycleObserver =
        object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun clearQueue() {
                requestHandler.removeMessages(MESSAGE_MAKE)
                requestMap.clear()
            }
        }

    private var hasQuit = false
    private lateinit var requestHandler: Handler
    private val requestMap = ConcurrentHashMap<T, String>()

    override fun quit(): Boolean {
        hasQuit = true
        return super.quit()
    }

    @Suppress("UNCHECKED_CAST")
    @SuppressLint("HandlerLeak")
    override fun onLooperPrepared() {
        requestHandler = object : Handler() {
            override fun handleMessage(msg: Message) {
                if (msg.what == MESSAGE_MAKE) {
                    val target = msg.obj as T
                    handleRequest(target)
                }
            }
        }
    }

    fun queueThumbnail(target: T, photoFilePath: String) {
        requestMap[target] = photoFilePath
        requestHandler.obtainMessage(MESSAGE_MAKE, target)
            .sendToTarget()
    }

    private fun handleRequest(target: T) {
        val photoFilePath = requestMap[target] ?: return
        val bitmap = getScaledBitmap(photoFilePath, 100, 100)

        responseHandler.post(Runnable {
            if (requestMap[target] != photoFilePath || hasQuit) {
                return@Runnable
            }

            requestMap.remove(target)
            onThumbnailMade(target, bitmap)
        })
    }
}