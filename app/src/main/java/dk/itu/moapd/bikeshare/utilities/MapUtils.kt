package dk.itu.moapd.bikeshare.utilities

import android.app.Activity
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import dk.itu.moapd.bikeshare.R
import dk.itu.moapd.bikeshare.entities.Bike
import kotlinx.android.synthetic.main.marker.view.*

class CustomInfoWindowGoogleMap(val activity: Activity) : GoogleMap.InfoWindowAdapter {

    override fun getInfoContents(marker: Marker?): View? {
        val mInfoView = activity.layoutInflater.inflate(R.layout.marker, null)
        val mBike = marker?.tag as Bike?
        mInfoView.marker_type.text = mBike?.type.toString()
        mInfoView.marker_price.text = mBike?.price.toString()

        return mInfoView
    }

    override fun getInfoWindow(marker: Marker?): View? {
        return null
    }
}