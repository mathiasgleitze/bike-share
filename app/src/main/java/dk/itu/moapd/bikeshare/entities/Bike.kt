package dk.itu.moapd.bikeshare.entities

import androidx.room.*
import java.util.*


@Entity
data class Bike(@PrimaryKey val bikeId: UUID = UUID.randomUUID(),
                var type: Type? = null,
                var price: Int = 0,
                var lat: Double = 0.0,
                var lng: Double = 0.0,
                var active: Boolean = false) {
    val photoFileName
        get() = "IMG_$bikeId.jpg"
}

enum class Type(val type: Int) {
    Mountain(0),
    City(1),
    Racer(2),
    Women(3);

    companion object {
        operator fun invoke(type: Int) = values().find { it.type == type }
    }
}
