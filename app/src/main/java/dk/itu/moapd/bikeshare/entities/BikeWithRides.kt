package dk.itu.moapd.bikeshare.entities

import androidx.room.Embedded
import androidx.room.Relation

data class BikeWithRides(
    @Embedded val bike: Bike,
    @Relation(
        parentColumn = "bikeId",
        entityColumn = "relatedBikeId"
    )
    val Rides: List<Ride>
)