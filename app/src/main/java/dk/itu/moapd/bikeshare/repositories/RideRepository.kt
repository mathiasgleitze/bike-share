package dk.itu.moapd.bikeshare.repositories

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import dk.itu.moapd.bikeshare.entities.Ride
import dk.itu.moapd.bikeshare.database.AppDatabase
import java.util.*
import java.util.concurrent.Executors

private const val DATABASE_NAME = "app-database"

class RideRepository private constructor(context: Context) {

    private val database : AppDatabase = Room.databaseBuilder(
        context.applicationContext,
        AppDatabase::class.java,
        DATABASE_NAME
    )
        .fallbackToDestructiveMigration()
        .build()

    private val rideDao = database.rideDao()
    private val executor = Executors.newSingleThreadExecutor()

    fun getRides(): LiveData<List<Ride>> = rideDao.getRides()

    fun getActiveRides(): LiveData<List<Ride>> = rideDao.getActiveRides()

    fun getRide(id: UUID): LiveData<Ride?> = rideDao.getRide(id)

    fun updateRide(ride: Ride) {
        executor.execute {
            rideDao.updateRide(ride)
        }
    }

    fun addRide(ride: Ride) {
        executor.execute {
            rideDao.addRide(ride)
        }
    }

    fun deleteRide(ride: Ride) {
        executor.execute {
            rideDao.deleteRide(ride)
        }
    }

    companion object {
        private var INSTANCE: RideRepository? = null
        fun initialize(context: Context) {
            if (INSTANCE == null) {
                INSTANCE =
                    RideRepository(context)
            }
        }
        fun get(): RideRepository {
            return INSTANCE
                ?:
                throw IllegalStateException("RideRepository must be initialized")
        }
    }
}