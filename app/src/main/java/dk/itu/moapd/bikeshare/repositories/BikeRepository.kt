package dk.itu.moapd.bikeshare.repositories

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import dk.itu.moapd.bikeshare.entities.Bike
import dk.itu.moapd.bikeshare.database.AppDatabase
import dk.itu.moapd.bikeshare.entities.BikeWithRides
import java.io.File
import java.util.*
import java.util.concurrent.Executors

private const val DATABASE_NAME = "app-database"

class BikeRepository private constructor(context: Context) {

    private val database : AppDatabase = Room.databaseBuilder(
        context.applicationContext,
        AppDatabase::class.java,
        DATABASE_NAME
    )
        .fallbackToDestructiveMigration()
        .build()

    private val bikeDao = database.bikeDao()
    private val executor = Executors.newSingleThreadExecutor()
    private val filesDir = context.applicationContext.filesDir

    fun getPhotoFile(bike: Bike): File = File(filesDir, bike.photoFileName)

    fun getBikes(): LiveData<List<Bike>> = bikeDao.getBikes()

    fun getBike(id: UUID): LiveData<Bike?> = bikeDao.getBike(id)

    fun getBikesWithRides(): LiveData<List<BikeWithRides>> = bikeDao.getBikesWithRides()

    fun getLat(bike: Bike): Double = bike.lat

    fun getLng(bike: Bike): Double = bike.lng

    fun updateBike(bike: Bike) {
        executor.execute {
            bikeDao.updateBike(bike)
        }
    }

    fun addBike(bike: Bike) {
        executor.execute {
            bikeDao.addBike(bike)
        }
    }

    fun deleteBike(bike: Bike) {
        executor.execute {
            bikeDao.deleteBike(bike)
        }
    }

    companion object {
        private var INSTANCE: BikeRepository? = null
        fun initialize(context: Context) {
            if (INSTANCE == null) {
                INSTANCE =
                    BikeRepository(context)
            }
        }
        fun get(): BikeRepository {
            return INSTANCE
                ?:
            throw IllegalStateException("BikeRepository must be initialized")
        }
    }
}