package dk.itu.moapd.bikeshare.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dk.itu.moapd.bikeshare.entities.Bike
import dk.itu.moapd.bikeshare.repositories.BikeRepository
import java.io.File

class RegisterBikeVM () : ViewModel() {

    private val bikeRepository: BikeRepository = BikeRepository.get()

    val bikeLiveData = MutableLiveData<Bike?>()

    fun loadBike(bike: Bike) {
        bikeLiveData.value = bike
    }

    fun getPhotoFile(): File {
        return bikeRepository.getPhotoFile(bikeLiveData.value!!)
    }

    fun addBike(bike: Bike) {
        bikeRepository.addBike(bike)
    }
}